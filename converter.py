# -*- coding: utf-8 -*-
"""
***** ***** *****
Grammar 1: a grammar for translating roman numerals to arabic numerals.
***** ***** *****

BNF                                             |   Semantic rules
================================================================================================
roman_numeral       =>  hundreds tens units     |   .val = hundreds.val + tens.val + units.val
                                                |
hundreds            =>  CM                      |   .val = 900
hundreds            =>  D lower_hundreds        |   .val = 500 + lower_hundreds.val
hundreds            =>  CD                      |   .val = 400
hundreds            =>  lower_hundreds          |   .val = lower_hundreds.val
lower_hundreds      =>  CCC                     |   .val = 300
lower_hundreds      =>  CC                      |   .val = 200
lower_hundreds      =>  C                       |   .val = 100
lower_hundreds      =>  (e)                     |   .val = 0
                                                |
tens                =>  XC                      |   .val = 90
tens                =>  L lower_tens            |   .val = 50 + lower_tens.val
tens                =>  XL                      |   .val = 40
tens                =>  lower_tens              |   .val = lower_tens.val
lower_tens          =>  XXX                     |   .val = 30
lower_tens          =>  XX                      |   .val = 20
lower_tens          =>  X                       |   .val = 10
lower_tens          =>  (e)                     |   .val = 0
                                                |
units               =>  IX                      |   .val = 9
units               =>  V lower_units           |   .val = 5 + lower_units.val
units               =>  IV                      |   .val = 4
units               =>  lower_units             |   .val = lower_units.val
lower_units         =>  III                     |   .val = 3
lower_units         =>  II                      |   .val = 2
lower_units         =>  I                       |   .val = 1
lower_units         =>  (e)                     |   .val = 0


***** ***** ***** ***** ***** ***** ***** ***** ***** ***** ***** ***** ***** ***** ***** *****
***** ***** ***** ***** ***** ***** ***** ***** ***** ***** ***** ***** ***** ***** ***** *****


***** ***** *****
Grammar 2: a modified grammar to be used by a syntax-driven predictive translator.
***** ***** *****

BNF                                             |   Semantic rules
================================================================================================
roman_numeral       =>  hundreds tens units     |   .val = hundreds.val + tens.val + units.val
                                                |
hundreds            =>  D hundreds_first_c      |   .val = 500 + hundreds_first_c.val
hundreds            =>  C hundreds_others       |   .val = hundreds_others.val
hundreds            =>  (e)                     |   .val = 0
hundreds_others     =>  M                       |   .val = 900
hundreds_others     =>  D                       |   .val = 400
hundreds_others     =>  hundreds_second_c       |   .val = 100 + hundreds_second_c.val
hundreds_first_c    =>  C hundreds_second_c     |   .val = 100 + hundreds_second_c.val
hundreds_first_c    =>  (e)                     |   .val = 0
hundreds_second_c   =>  C hundreds_third_c      |   .val = 100 + hundreds_third_c.val
hundreds_second_c   =>  (e)                     |   .val = 0
hundreds_third_c    =>  C                       |   .val = 100
hundreds_third_c    =>  (e)                     |   .val = 0
                                                |
tens                =>  L tens_first_x          |   .val = 50 + tens_first_x.val
tens                =>  X tens_others           |   .val = tens_others.val
tens                =>  (e)                     |   .val = 0
tens_others         =>  C                       |   .val = 90
tens_others         =>  L                       |   .val = 40
tens_others         =>  tens_second_x           |   .val = 10 + tens_second_x.val
tens_first_x        =>  X tens_second_x         |   .val = 10 + tens_second_x.val
tens_first_x        =>  (e)                     |   .val = 0
tens_second_x       =>  X tens_third_x          |   .val = 10 + tens_third_x.val
tens_second_x       =>  (e)                     |   .val = 0
tens_third_x        =>  X                       |   .val = 10
tens_third_x        =>  (e)                     |   .val = 0
                                                |
units               =>  V units_first_i         |   .val = 5 + units_first_i.val
units               =>  I units_others          |   .val = units_others.val
units               =>  (e)                     |   .val = 0
units_others        =>  X                       |   .val = 9
units_others        =>  V                       |   .val = 4
units_others        =>  units_second_i          |   .val = 1 + units_second_i.val
units_first_i       =>  I units_second_i        |   .val = 1 + units_second_i.val
units_first_i       =>  (e)                     |   .val = 0
units_second_i      =>  I units_third_i         |   .val = 1 + units_third_i.val
units_second_i      =>  (e)                     |   .val = 0
units_third_i       =>  I                       |   .val = 1
units_third_i       =>  (e)                     |   .val = 0
"""

# Copyright (C) 2016 Nico Epp and Ralf Funk
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import sys


class Converter:

    def __init__(self, input_str):
        self._input_str = input_str.upper()
        self._current_char = 0

    @property
    def current_char(self):
        return self._input_str[self._current_char:self._current_char+1]

    def _match(self):
        self._current_char += 1

    def convert(self):
        # roman_numeral =>  hundreds tens units   |   .val = hundreds.val + tens.val + units.val
        self._current_char = 0
        aux = self._hundreds() + self._tens() + self._units()

        input_remainder = self._input_str[self._current_char:]
        if input_remainder:
            raise ValueError('Unexpected token "{}" at pos {}'.format(
                self.current_char, self._current_char))

        return aux

    def _hundreds(self):
        # hundreds            =>  D hundreds_first_c      |   .val = 500 + hundreds_first_c.val
        # hundreds            =>  C hundreds_others       |   .val = hundreds_others.val
        # hundreds            =>  (e)                     |   .val = 0
        if self.current_char == 'D':
            self._match()
            return 500 + self._hundreds_first_c()
        elif self.current_char == 'C':
            self._match()
            return self._hundreds_others()
        else:
            return 0

    def _hundreds_others(self):
        # hundreds_others     =>  M                       |   .val = 900
        # hundreds_others     =>  D                       |   .val = 400
        # hundreds_others     =>  hundreds_second_c       |   .val = 100 + hundreds_second_c.val
        if self.current_char == 'M':
            self._match()
            return 900
        elif self.current_char == 'D':
            self._match()
            return 400
        else:
            return 100 + self._hundreds_second_c()

    def _hundreds_first_c(self):
        # hundreds_first_c    =>  C hundreds_second_c     |   .val = 100 + hundreds_second_c.val
        # hundreds_first_c    =>  (e)                     |   .val = 0
        if self.current_char == 'C':
            self._match()
            return 100 + self._hundreds_second_c()
        else:
            return 0

    def _hundreds_second_c(self):
        # hundreds_second_c   =>  C hundreds_third_c      |   .val = 100 + hundreds_third_c.val
        # hundreds_second_c   =>  (e)                     |   .val = 0
        if self.current_char == 'C':
            self._match()
            return 100 + self._hundreds_third_c()
        else:
            return 0

    def _hundreds_third_c(self):
        # hundreds_third_c    =>  C                       |   .val = 100
        # hundreds_third_c    =>  (e)                     |   .val = 0
        if self.current_char == 'C':
            self._match()
            return 100
        else:
            return 0

    def _tens(self):
        # tens                =>  L tens_first_x          |   .val = 50 + tens_first_x.val
        # tens                =>  X tens_others           |   .val = tens_others.val
        # tens                =>  (e)                     |   .val = 0
        if self.current_char == 'L':
            self._match()
            return 50 + self._tens_first_x()
        elif self.current_char == 'X':
            self._match()
            return self._tens_others()
        else:
            return 0

    def _tens_others(self):
        # tens_others         =>  C                       |   .val = 90
        # tens_others         =>  L                       |   .val = 40
        # tens_others         =>  tens_second_x           |   .val = 10 + tens_second_x.val
        if self.current_char == 'C':
            self._match()
            return 90
        elif self.current_char == 'L':
            self._match()
            return 40
        else:
            return 10 + self._tens_second_x()

    def _tens_first_x(self):
        # tens_first_x        =>  X tens_second_x         |   .val = 10 + tens_second_x.val
        # tens_first_x        =>  (e)                     |   .val = 0
        if self.current_char == 'X':
            self._match()
            return 10 + self._tens_second_x()
        else:
            return 0

    def _tens_second_x(self):
        # tens_second_x       =>  X tens_third_x          |   .val = 10 + tens_third_x.val
        # tens_second_x       =>  (e)                     |   .val = 0
        if self.current_char == 'X':
            self._match()
            return 10 + self._tens_third_x()
        else:
            return 0

    def _tens_third_x(self):
        # tens_third_x        =>  X                       |   .val = 10
        # tens_third_x        =>  (e)                     |   .val = 0
        if self.current_char == 'X':
            self._match()
            return 10
        else:
            return 0

    def _units(self):
        # units               =>  V units_first_i         |   .val = 5 + units_first_i.val
        # units               =>  I units_others          |   .val = units_others.val
        # units               =>  (e)                     |   .val = 0
        if self.current_char == 'V':
            self._match()
            return 5 + self._units_first_i()
        elif self.current_char == 'I':
            self._match()
            return self._units_others()
        else:
            return 0

    def _units_others(self):
        # units_others        =>  X                       |   .val = 9
        # units_others        =>  V                       |   .val = 4
        # units_others        =>  units_second_i          |   .val = 1 + units_second_i.val
        if self.current_char == 'X':
            self._match()
            return 9
        elif self.current_char == 'V':
            self._match()
            return 4
        else:
            return 1 + self._units_second_i()

    def _units_first_i(self):
        # units_first_i       =>  I units_second_i        |   .val = 1 + units_second_i.val
        # units_first_i       =>  (e)                     |   .val = 0
        if self.current_char == 'I':
            self._match()
            return 1 + self._units_second_i()
        else:
            return 0

    def _units_second_i(self):
        # units_second_i      =>  I units_third_i         |   .val = 1 + units_third_i.val
        # units_second_i      =>  (e)                     |   .val = 0
        if self.current_char == 'I':
            self._match()
            return 1 + self._units_third_i()
        else:
            return 0

    def _units_third_i(self):
        # units_third_i       =>  I                       |   .val = 1
        # units_third_i       =>  (e)                     |   .val = 0
        if self.current_char == 'I':
            self._match()
            return 1
        else:
            return 0


def convert_from_roman_numeral(s):
    return Converter(s).convert()


if __name__ == '__main__':
    print()
    print('{:>3s} | {:20s} | {}'.format('#', 'Roman numeral', 'Number'))
    print('-' * 37)

    for i, e in enumerate(sys.argv[1:]):
        try:
            r = convert_from_roman_numeral(e)
            print('{:3d} | {:>20s} | {:7d}'.format(i+1, e, r))
        except ValueError as err:
            print('{:3d} | {:>20s} | {}'.format(i+1, e, err))
