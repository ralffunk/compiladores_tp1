# -*- coding: utf-8 -*-

# Copyright (C) 2016 Nico Epp and Ralf Funk
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import unittest
from converter import convert_from_roman_numeral


class MyTest(unittest.TestCase):

    def test_correct_numerals(self):
        with open('correct_numerals.txt') as f:
            for i, line in enumerate(f):
                line = line.strip()

                if line:
                    arabic_numeral, roman_numeral = line.split()

                    try:
                        arabic_numeral = int(arabic_numeral)
                    except ValueError:
                        pass

                    with self.subTest(i=i):
                        self.assertEqual(
                            convert_from_roman_numeral(roman_numeral),
                            arabic_numeral)

    def test_invalid_numerals(self):
        with open('invalid_numerals.txt') as f:
            for i, line in enumerate(f):
                line = line.strip()

                if line and not line.startswith('#'):
                    with self.subTest(i=i):
                        with self.assertRaises(ValueError):
                            convert_from_roman_numeral(line)


if __name__ == '__main__':
    unittest.main()
